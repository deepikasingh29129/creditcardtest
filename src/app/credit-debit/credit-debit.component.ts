import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-credit-debit',
  templateUrl: './credit-debit.component.html',
  styleUrls: ['./credit-debit.component.css']
})
export class CreditDebitComponent implements OnInit {
  creditForm:FormGroup;
  constructor( private api:ApiService,private fb:FormBuilder) {
    this.creditForm=this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')])],
      cardNo: ['', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')])],
      cvv: ['', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')])],
      expiryMonth: ['', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')])],
      expiryYear: ['', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')])],
    })
   }

  ngOnInit(): void {
  }
  onSubmit(){
    console.log(this.creditForm);
    let reqObj=this.creditForm.value;
    this.api.post(environment.endpoint,reqObj).then(resp=>{alert(JSON.stringify(resp))},err=>{alert(JSON.stringify(err))});
  }

}
