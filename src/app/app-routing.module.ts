import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditDebitComponent } from './credit-debit/credit-debit.component';

const routes: Routes = [
  { 
    path: '', 
    component: CreditDebitComponent,
  }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {  }
